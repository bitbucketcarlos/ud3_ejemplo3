# Ud3_Ejemplo3
_Ejemplo 3 de la Unidad 3._

Creamos _Intents_ explícitos y abrimos varias actividades al igual que en [Ud2_Ejemplo2](https://bitbucket.org/bitbucketcarlos/ud3_ejemplo2) pero en este caso no hacemos uso del 
atributo _onClick_ en los _TextViews_ sino que utilizamos _onClickListener_.

Los pasos a seguir son parecidos al ejemplo anterior:

## Paso 1: Creación de los layouts

Primero creamos el layout de la Actividad principal en el fichero _activity_main.xml_:
```html
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

    <TextView
        android:id="@+id/actividad1"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="@android:color/holo_blue_dark"
        android:padding="16dp"
        android:text="@string/actividad_1"
        android:textColor="@android:color/white" />

    <TextView
        android:id="@+id/actividad2"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="@android:color/holo_green_dark"
        android:padding="16dp"
        android:text="@string/actividad_2"
        android:textColor="@android:color/white" />

</LinearLayout>
```
En él creamos dos _TextViews_ que al pulsarlos abrirán las actividades. Notad que no está el atributo _onClick_ y que cada
_TextView_ tiene su _id_ asociado, esto es necesario para posteriormente poder buscarlo y asignarle el _Click Listener_.

Después creamos los layouts de las dos actividades. Para crearlos podemos hacerlo directamente clicando con el botón derecho sobre el paquete de la aplicación y 
despues sobre _New > Activity > Empty Activity_ o desde el menú crear dos nuevos archivos _Layout resource files_ dentro de la carpeta _values_:

_actividad1.xml_:
```html
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".Actividad1">

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/actividad_1"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```
y _actividad2.xml_:
```html
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".Actividad2">

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/actividad_2"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```
## Paso 2: Creación de las actividades
Ahora vamos a crear las actividades. Si hemos creado las dos actividades vacías (_Empty Activity_) ya los tendremos creados sino debemos dos clases que extiendan de la clase _AppCompatActivity_ y sobreescriban la función _onCreate_. 
En él se cargará el _layout_ de la actividad usando la función _setContentView_ y accediendo a él a través de la clase _R_.

_Actividad1.kt_:
```java
class Actividad1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actividad1)
    }
}
```
_Actividad2.kt_:
```java
class Actividad2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actividad2)
    }
}
```
## Paso 3: Asignación de los _Click Listeners_ a los _TextView_
El siguiente paso es modificar la clase _MainActivity.java_ y asignar a los dos _TextView_ de nuestra aplicación los
_Click Listeners_. Para ello hay que hacer:

1. Implementar el _ViewBinding_ de la aplicación.
2. Asignar el _Click Listener_ sobre ese _TextView_.
3. Crear y lanzar el _Intent_.

Recuerda que para implementar el _ViewBinding_ deberemos añadir las siguientes líneas de código en el fichero _build.gradle:app_ y sincronizarlo:
```java
    viewBinding {
        enabled = true
    }
```

_MainActivity.kt_:
```java
class MainActivity : AppCompatActivity() {

    // Inicialización tardía. Para evitar el valor nulo de la variable ya que ésta se crea más tarde.
    // 'lateinit' para var y 'by lazy' para val
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Obtenemos el "binding" e inflamos el layout.
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Accedemos a los botones y les asignamos el Listener
        binding.actividad1.setOnClickListener {
            val intentAct1 = Intent(this, Actividad1::class.java)
            startActivity(intentAct1)
        }

        binding.actividad2.setOnClickListener {
            val intentAct2 = Intent(this, Actividad2::class.java)
            startActivity(intentAct2)
        }

    }
}
```
## Paso 4: Inserción de las actividades en el fichero _AndroidManifest.xml_
Por último, tenemos que indicarle al fichero _AndroidManifest.xml_ que las nuevas actividades existen y tienen como actividad padre a _MainActivity.java_. 
Para ello añadimos las siguientes líneas (o modificamos las introducidas al crear las actividades vacías):
```html
...
        <activity
            android:name=".Actividad2"
            android:exported="true"
            android:label="@string/actividad_2"
            android:parentActivityName=".MainActivity"/>
        <activity
            android:name=".Actividad1"
            android:exported="true"
            android:label="@string/actividad_1"
            android:parentActivityName=".MainActivity"/>
    </application>
</manifest>
```
