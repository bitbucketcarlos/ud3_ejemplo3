package com.example.ud3_ejemplo3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.ud3_ejemplo3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    // Inicialización tardía. Para evitar el valor nulo de la variable ya que ésta se crea más tarde.
    // 'lateinit' para var y 'by lazy' para val
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Obtenemos el "binding" e inflamos el layout.
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Accedemos a los botones y les asignamos el Listener
        binding.actividad1.setOnClickListener {
            val intentAct1 = Intent(this, Actividad1::class.java)
            startActivity(intentAct1)
        }

        binding.actividad2.setOnClickListener {
            val intentAct2 = Intent(this, Actividad2::class.java)
            startActivity(intentAct2)
        }

    }
}